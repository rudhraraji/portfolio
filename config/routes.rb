Rails.application.routes.draw do
  
  root 'pages#home'
  	get 'home', to: 'pages#home'
	get 'skill', to: 'pages#skill'
	get 'experience', to:'pages#experience'
	get 'education', to:'pages#education'
	get 'portfolio', to: 'pages#portfolio'
	get 'contact', to: 'pages#contact'
	get 'docx', to: 'pages#docx'
	get 'zip', to: 'pages#zip'
	get 'pdf', to: 'pages#pdf'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
