class PagesController < ApplicationController
  def home
  end

  def skill
  end

  def education
  end

  def experience
  end

  def portfolio
  end

  def pdf
  end

  def docx
  end

  def zip
  end
end
